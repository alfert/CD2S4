\babel@toc {english}{}
\contentsline {section}{\numberline {1}Preface}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Terminology}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Definitions}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Usage}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Installation}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Dependencies}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Method overview}{4}{section.3}
\contentsline {section}{\numberline {4}Algorithm}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Initialisation}{5}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}The unit cell}{5}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Rotations and orientations}{5}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Everything else}{6}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Intensity calculations}{6}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}The Bragg peaks}{7}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}The structure factor}{7}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}The beamstop}{7}{subsubsection.4.2.3}
\contentsline {subsection}{\numberline {4.3}Plotting the image}{8}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Drawing the schematic}{8}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Tidying up}{8}{subsection.4.5}
\contentsline {section}{\numberline {5}Recommendations for future progress}{8}{section.5}
\contentsline {section}{\numberline {6}Postface}{9}{section.6}
\contentsline {section}{\numberline {7}Legal stuff}{10}{section.7}

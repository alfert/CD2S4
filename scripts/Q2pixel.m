function [X,Y] = Q2pixel(Q,D,W,N,theta)
% Q2pixel Returns the fractional pixel which recieves a given Q.
%   
%   Inputs:
%       Q       The directional scattering vector, without the wavenumber
%               factor.
%       D       Distance from the sample to the detector in mm.
%       W       Width of the detector's active region in mm.
%       N       Number of pixels along each side of the detector.
%       theta   The angle a line from the sample to the detector's centre
%               makes with the x-axis in degrees.
%
%   Outputs:
%       X, Y    The X- and Y-coordinates of the incident pixel.
%
%   Notes:
%       -       The pixel (1,1) are set to the lower-left corner of the
%               detector (-y, -z direction in direct space), and the direct
%               space origin is set at the sample.
%       -       Incident radiation moves in the negative x-direction.
%       -       If no pixel recieves radiation of the given Q, then the
%               function reports an error. Use a try...catch block to work
%               around this.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



Q = genrot(theta,[0;0;1]) ...
    * (reshape(Q,3,1)-genrot(-0.08756,[0;1;0])*[1;0;0]);
Q = -Q/Q(1)*D;                      % Scale to touch detector

X = Q(2)*(N-1)/W + 1 + (N-1)/2;
Y = Q(3)*(N-1)/W + 1 + (N-1)/2;

if X<1 || Y<1 || X>N || Y>N
    error('No pixel recieves given Q vector!');
end



end
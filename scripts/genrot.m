function [R] = genrot(angle,axis)
%GENROT General rotation matrix.
%   GENROT(angle,axis) returns a 3x3 real matrix corresponding to a right-
%   handed rotation about axis by angle degrees.
%
%   Inputs:
%       angle   A real number.
%       axis    A real 3D row or column vector.
%
%   Outputs:
%       R       The rotation matrix.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



n = axis/norm(axis);    % Normalise axial vector
c = 1-cosd(angle);
s = sind(angle);

% General matrix
R = [    1-c+c*n(1)^2    , c*n(1)*n(2)-s*n(3) , c*n(1)*n(3)+s*n(2) ;
      c*n(1)*n(2)+s*n(3) ,    1-c+c*n(2)^2    , c*n(2)*n(3)-s*n(1) ;
      c*n(1)*n(3)-s*n(2) , c*n(2)*n(3)+s*n(1) ,    1-c+c*n(3)^2    ];



end
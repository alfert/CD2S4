function [I] = structfact(Q,atoms)
%STRUCTFACT Calculates intensity envelope due to the structure factor.
%
%   Inputs:
%       Q           A 3D column vector in reciprocal space, measured in
%                   reciprocal Ang.
%       atoms       An 3xN matrix where each column represents the
%                   coordinates of the basis' atoms in direct space,
%                   measured in Ang.
%
%   Outputs:
%       I           The normalised intensity due to the structure factor.
%
%   Notes:
%       -           Q-dependent form factors are calculated using an
%                   approxiation as the sum of four Gaussians.
%                   Source:
%                   	https://lampx.tugraz.at/~hadley/ss1/
%                           crystaldiffraction/atomicformfactors/
%                           formfactors.php
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



q = norm(Q);
Isum = 0;

for j=1:16                          % Yb 3+
    formfact = 2.26001 + 27.8917*exp(-1.73272*(q/4/pi())^2) ...
                       + 18.7614*exp(-0.13879*(q/4/pi())^2) ...
                       + 12.6072*exp(-7.64412*(q/4/pi())^2) ...
                       + 5.47647*exp(-16.8153*(q/4/pi())^2);
    Isum = Isum + formfact*exp(1i*dot(Q,atoms(:,j)));
end

for j=17:32                         % Ti 4+
    formfact = -13.280 + 19.5114*exp(-0.178847*(q/4/pi())^2) ...
                       + 8.23473*exp(-6.67018*(q/4/pi())^2) ...
                       + 2.01341*exp(0.29263*(q/4/pi())^2) ...
                       + 1.5208*exp(-12.9464*(q/4/pi())^2);
    Isum = Isum + formfact*exp(1i*dot(Q,atoms(:,j)));
end

for j=33:88                         % O 2-
    formfact = 0.242060 + 3.75040*exp(-2.84294*(q/4/pi())^2) ...
                        + 1.54298*exp(-1.62091*(q/4/pi())^2) ...
                        + 16.5151*exp(-6.59203*(q/4/pi())^2) ...
                        + 0.319201*exp(-43.3486*(q/4/pi())^2);
    Isum = Isum + formfact*exp(1i*dot(Q,atoms(:,j)));
end

I = abs(Isum)^2;



end
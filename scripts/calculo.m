% CALCULO runs the full direct-space XRD simulation.
%
%   Predefine:
%       latparam    A 3x1 vector whose components represent the lattice
%                   constants, in nm, for the a-, b-, and c- vectors
%                   respectively (default: ybtio).
%       r           An Nx3 matrix whose columns represent the fractional
%                   a-, b-, and c- coordinates of the N atoms in the basis,
%                   listed as rows (default: ybtio).
%       W           Length of detector's square active region in mm
%                   (default: 63.6 mm).
%       D           Distance from sample to detector's centre in mm
%                   (default: 41 mm).
%       lambda      Wavelength of incident light in Ang (default:
%                   0.71073 Ang).
%       geometry    A flag, accepting either 'euler' or 'kappa',
%                   corresponding to each goniometer angle convention.
%       omega       Angle the base turns in degrees (default: 0 deg).
%       chi/kappa   Angle the arm turns in degrees (default: 0 deg).
%       phi         Angle the head turns in degrees (default: 0 deg).
%       theta       Angle a line from the sample to the detector's centre
%                   makes with the x-axis in degrees (default: 0 deg).
%       U           The orientation matrix (default: eye(3)).
%       N           Side-length of output image in pixels (default: 255).
%       FWHM        The FWHM of a Bragg peak due to the resolution of the
%                   instrument, measured in Q-space Ang^-1 (default: 0.05).
%       threshold   A scalar between 0 and 1; it scales the cut-off point
%                   for negligible intensities (default: 0.01).
%
%   Optional flags (set true to enable):
%       nocalc      Suppresses the calculation subroutine. Initialisation
%                   will still occur.
%       noplot      Suppresses the plotting subroutine.
%       nodraw      Suppresses the subroutine which draws a 3D diagram of
%                   the XRD settings.
%
%   Note:
%       -           The alpha angle (for kappa arm) can be tuned on line
%                   130 and the beta angle can be tuned on line 129.
%
%   Author:         ---
%   Email:          ---
%   Date:           13/11/2018



tic;



% INITIALISATION 1 -- CHECK DEFINED CONSTANTS

loadbar = waitbar(0,'Initialising...');
if exist('latparam','var')==0
    warning("Lattice parameter (latparam) not defined; assuming ybtio.");
    ybtio;                      % See 'ybtio.m' script.
end
if exist('r','var')==0
    warning("Atomic positions (r) not defined; assuming ybtio.");
    ybtio;                      % See 'ybtio.m' script.
end
if exist('W','var')==0
    warning("Detector width (W) not defined; assuming 63.6 mm.");
    W = 63.6;
end
if exist('D','var')==0
    warning("Detector distance (D) not defined; assuming 41 mm.");
    D = 41;
end
if exist('lambda','var')==0
    warning("Wavelength (lambda) not defined; assuming 0.71073 Ang.");
    lambda = 0.71073;
end
if exist('theta','var')==0
    warning("Detector angle (theta) not defined; assuming 0 deg.");
    theta = 0;
end
if exist('N','var')==0
    warning("Pixel number (N) not defined; assuming 256^2.");
    N = 256;
elseif mod(N,2)==1
    N = N-1;
end
if exist('FWHM','var')==0
    warning("Bragg peak width (FWHM) not defined; assuming 0.05 Ang^-1.");
    FWHM = 0.05;                % Somewhat of an arbitrary scale. 0.02-0.1
end                             % is a good range to use.
if exist('threshold','var')==0
    warning("Negligible intensity point not defined; assuming 0.01.");
    threshold = 0.05;
end
if exist('omega','var')==0
    warning("Omega angle (omega) not defined; assuming 0 deg.");
    omega = 0;
end
if exist('geometry','var')==0
    warning("Geometry convention not defined; assuming 'kappa'.");
    geometry = 'kappa';
end
if strcmp(geometry,'euler')==1
    clear kappa;
    if exist('chi','var')==0
        warning("Chi angle (chi) not defined; assuming 0 deg.");
        chi = 0;
    end
    angle2 = chi;
elseif strcmp(geometry,'kappa')==1
    clear chi;
    if exist('kappa','var')==0
        warning("Kappa angle (kappa) not defined; assuming 0 deg.");
        kappa = 0;
    end
    angle2 = kappa;
end
if exist('phi','var')==0
    warning("Phi angle (phi) not defined; assuming 0 deg.");
    phi = 0;
end
if exist('U','var')==0
    warning("Orientation matrix (U) not defined; assuming eye(3).");
    U = eye(3);
end



% INITIALISATION 2 -- CALCULATE DERIVED CONSTANTS

waitbar(1/64,loadbar);
if strcmp(geometry,'euler')==1
    InstrRot = genrot(-omega,[0;0;1]) ... 
               * genrot(chi,[1,0,0]) ...
               * genrot(-phi,[0;0;1]);
elseif strcmp(geometry,'kappa')==1
    InstrRot = genrot(-omega,[0;0;1]) ...
               * genrot(-kappa, ...
                        ...%genrot(0.00593,[0;0;1]) * ...
                        genrot(-50.05885,[0;1;0]) * [0;0;1]) ...
               * genrot(-phi,[0;0;1]);
end

a = InstrRot*U*[latparam(1);0;0];  % Direct lattice basis vectors
b = InstrRot*U*[0;latparam(2);0];
c = InstrRot*U*[0;0;latparam(3)];
atoms = InstrRot*U*(repmat(latparam',1,length(r)).*r');
                                   % Unit cell positions in direct space

[A,B,C] = reciprogen(a,b,c);       % Reciprocal lattice basis vectors
k = 2*pi()/lambda;                 % Convert to wavenumber



% MAIN CALCULATION

if exist('nocalc','var')==1
    if nocalc==false
        [I,X0,Y0] = docomputation(A,B,C,D,FWHM,threshold,...
                                  k,N,W,theta,atoms,loadbar);
    else
        waitbar(1/32,loadbar,'Skipping calculation...');
        X0 = NaN;
        Y0 = NaN;
    end
else
    [I,X0,Y0] = docomputation(A,B,C,D,FWHM,threshold,...
                              k,N,W,theta,atoms,loadbar);
end



% PLOT GRAPH

if exist('noplot','var')==1
    if noplot==false
        waitbar(60/64,loadbar,'Plotting graph...');
        plotgraph(I,X0,Y0,geometry,omega,theta,angle2,phi,D);
    else
        waitbar(60/64,loadbar,'Skipping graph plot...');
    end
else
    waitbar(60/64,loadbar,'Plotting graph...');
    plotgraph(I,X0,Y0,geometry,omega,theta,angle2,phi,D);
end



% DRAW SCHEMATIC

if exist('nodraw','var')
    if nodraw==false
        waitbar(62/64,loadbar,'Drawing schematic...');
        drawXRD(U,InstrRot,theta);
    else
        waitbar(62/64,loadbar,'Skipping schematic drawing...');
    end
else
    waitbar(62/64,loadbar,'Drawing schematic...');
    drawXRD(U,InstrRot,theta);
end
 


% TIDY UP

waitbar(1,loadbar,'Tidying up...');
clear angle2;
clear X0;
clear Y0;
clear parprocqueue;
fprintf(['   \x2713    Elapsed time: ', ...         % Report time taken to
         num2str(round(toc,1)), ' seconds.\n']);    % complete calculation
waitbar(1,loadbar,'Done.');
close(loadbar);
clear loadbar;
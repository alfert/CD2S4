function [Q] = pixel2Q(X,Y,D,W,N,theta)
% PIXEL2Q Returns the unit Q vector incident on a detector pixel.
%   
%   Inputs:
%       X       x-coordinate of pixel.
%       Y       y-coordinate of pixel.
%       D       Distance from the sample to the detector in mm.
%       W       Width of the detector's active region in mm.
%       N       Number of pixels along each side of the detector.
%       theta   The angle a line from the sample to the detector's centre
%               makes with the x-axis in degrees.
%
%   Outputs:
%       Q       A 3D vector corresponding to the scattering vector with
%               which radiation is incident on the given pixel.
%
%   Notes:
%       -       The pixel (1,1) is set to the lower-left corner of the
%               detector (-y, -z direction in direct space), and the direct
%               space origin is set at the sample.
%       -       The output does not include the wavenumber factor.
%       -       Small misalignments of the detector surface may be added
%               via the genrot() functions on lines 35-36.
%       -       Small misalignments of the beam may be added via the
%               the genrot() functions on line 40.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



% Calculate final wavevector
Kf = genrot(-theta,[0;0;1]) ...
     * genrot(0.4051141191502955,[0;1;0]) ...       % y misalignment
     * genrot(0.01551450779960603,[1;0;0]) ...      % x misalignment
     * [-D ; (2*X-1-N)*W/N/2 ; (2*Y-1-N)*W/N/2];

% Normalise and subtract incident wavevector
Q = Kf/norm(Kf) - genrot(0.08756,[0;1;0])*[-1;0;0];



end
function [A,B,C] = reciprogen(a,b,c)
% RECIPROGEN Calculates reciprocal lattice vectors.
%
%   Inputs:
%       a,b,c   Three non-coplanar 3D vectors.
%
%   Outputs:
%       A,B,C   Three 3D vectors in reciprocal space.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



v = dot(a,cross(b,c));

A = 2*pi()*cross(b,c)/v;
B = 2*pi()*cross(c,a)/v;
C = 2*pi()*cross(a,b)/v;



end
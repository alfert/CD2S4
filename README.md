All the information you need can be found in `documentation.pdf`, which is (unsurprisingly) this project's documentation. Go read it!

Feel free to email me if you have any questions.
function [I,X0,Y0]=docomputation(A,B,C,D,FWHM,threshold,k,N,W,theta,atoms,loadbar)
%DOCOMPUTATION Executes the direct-space XRD simulation.
%
%   Use of this function solitarily is not recommended. Execute through the
%   CALCULO script with the suitable flags to do the calculation.
%
%   This function is a hack to allow the loading bar to update during the
%   parallelised calculation. See the CALCULO script for guidance on
%   setting the correct inputs.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



waitbar(1/32,loadbar,'Initialising parallel processor pool...');
gcp;

I(N,N) = 0;
counter = 1;

waitbar(3/64,loadbar,'Calculating intensities...');

parprocqueue = parallel.pool.DataQueue; % Call updatewaitbar() when the 
afterEach(parprocqueue,@updatewaitbar); % parprocqueue signal is received.

parfor Y=1:N                            % Y pixel coordinate
    for X=1:N                           % X pixel coordinate
        Q = k*pixel2Q(X,Y,D,W,N,theta); % Calculate Q vector
        bragg = braggcalc(Q,A,B,C,FWHM);% Calculate Bragg intensity
        if bragg<threshold
            I(Y,X) = 0;
        else
            I(Y,X) = bragg * structfact(Q,atoms);
        end
    end
    send(parprocqueue,0);               % Send parprocqueue signal
end

waitbar(59/64,loadbar);

try                                     % Place the beamstop if possible
    [X0,Y0] = Q2pixel([0,0,0]',D,W,N,theta);
    beamstop = N/24/1.5;
    I(ceil(Y0-beamstop):floor(Y0+beamstop),...
      ceil(X0-beamstop):floor(X0+beamstop)) = 0;
catch
    X0 = NaN;                           % Return NaN if beamstop is not in
    Y0 = NaN;                           % frame
end



function updatewaitbar(~)               % Subfunction to update loading bar
    waitbar(3/64+7*counter/N/8,loadbar);
    counter = counter + 1;
end



end
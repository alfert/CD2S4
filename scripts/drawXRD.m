function [] = drawXRD(U,InstrRot,theta)
%DRAWXRD Draws a diagram of the XRD's setup.
%
%   Inputs:
%       U           A rotation matrix which transforms the crystal axes,
%                   initially parallel to the XRD axes, to their
%                   orientaiton relative to the XRD with all settings at 0.
%       InstrRot    A rotation matrix which transforms the sample from its
%                   default position to its orientation upon measurement.
%       theta       The detector angle in degrees.
%
%   Author: ---
%   Email:  ---
%   Date:   27/07/2018



figure();
hold on

% Set and rotate detector corners (1:5,:) and centre (6,:)
DetCoords = [ -8 , -8 , -8 , -8 , -8 , -8 ;
              -4 , -4 ,  4 ,  4 , -4 ,  0 ;
              -4 ,  4 ,  4 , -4 , -4 , -5 ];
DetCoords = genrot(-theta,[0;0;1]) * DetCoords;

% Set text
text(0,0,-3,'bulk crystal','HorizontalAlignment','center');
text(0,0,-4,'& basis axes','HorizontalAlignment','center');
text(4,0,-1,'direct axes','HorizontalAlignment','center');
text(DetCoords(1,6),DetCoords(2,6),DetCoords(3,6), ...
     'detector','HorizontalAlignment','center');
 
% Draw detector
patch(DetCoords(1,1:5),DetCoords(2,1:5),DetCoords(3,1:5), ...
      'k','FaceAlpha',3/4);

% Draw XRD system coordinate axes
xhand = mArrow3([4;0;0],[8;0;0], ...
                'color','r','stemWidth',0.1,'tipWidth',0.25);
yhand = mArrow3([4;0;0],[4;4;0], ...
                'color','b','stemWidth',0.1,'tipWidth',0.25);
zhand = mArrow3([4;0;0],[4;0;4], ...
                'color','g','stemWidth',0.1,'tipWidth',0.25);

% Create, rotate, and draw crystal cylinder
[X,Y,Z] = cylinder(1,12);
Z = 4*Z - 2;
parfor j=1:2
    for k=1:13
        v = [X(j,k);Y(j,k);Z(j,k)];
        v = InstrRot*v;
        X(j,k) = v(1);
        Y(j,k) = v(2);
        Z(j,k) = v(3);
    end
end
surf(X,Y,Z,'FaceAlpha',1/4,'FaceColor','k','EdgeColor','none');

% Draw cylinder end to show the 'top'
fill3(X(2,:),Y(2,:),Z(2,:),'k','FaceAlpha',1/4);

% Draw marker lines on cylinder to show the 'front'
plot3([X(1,1),X(2,1),(X(2,1)+X(2,7))/2], ...
      [Y(1,1),Y(2,1),(Y(2,1)+Y(2,7))/2], ...
      [Z(1,1),Z(2,1),(Z(2,1)+Z(2,7))/2],'k--');

% Draw bulk crystal coordinate axes
mArrow3(InstrRot*[1;0;0],InstrRot*[3;0;0], ...
        'color','r','stemWidth',0.075,'tipWidth',0.1875,'FaceAlpha',1/6);
mArrow3(InstrRot*[0;1;0],InstrRot*[0;3;0], ...
        'color','b','stemWidth',0.075,'tipWidth',0.1875,'FaceAlpha',1/4);
mArrow3(InstrRot*[0;0;2],InstrRot*[0;0;4], ...
        'color','g','stemWidth',0.075,'tipWidth',0.1875,'FaceAlpha',1/4);

% Draw crystal basis coordinate axes
mArrow3([0;0;0],InstrRot*U*[2;0;0], ...
        'color','r','stemWidth',0.05,'tipWidth',0.125);
mArrow3([0;0;0],InstrRot*U*[0;2;0], ...
        'color','b','stemWidth',0.05,'tipWidth',0.125);
mArrow3([0;0;0],InstrRot*U*[0;0;2], ...
        'color','g','stemWidth',0.05,'tipWidth',0.125);

% Find and set axes limits
axis([min([DetCoords(1,1),DetCoords(1,3),-2])-1 , ...
      max([DetCoords(1,1),DetCoords(1,3),8])+1 , ... 
      min([DetCoords(2,1),DetCoords(2,3),-2])-1 , ...
      max([DetCoords(2,1),DetCoords(2,3),4])+1 , ...
      -5 , 6]);

% Formatting
axis equal;
grid;
view(135,rad2deg(atan(1/sqrt(2))));
xlabel('x');
ylabel('y');
zlabel('z');
legend([xhand,yhand,zhand],'x','y','z');
set(gca,'XTickLabel',[],'YTickLabel',[],'ZTickLabel',[], ...
    'TickLength',[0,0]);

hold off



end
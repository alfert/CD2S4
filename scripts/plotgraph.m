function plotgraph(I,X0,Y0,geometry,omega,theta,angle2,phi,D)
%PLOTGRAGH Plots the output graph
%
%   Use of this function solitarily is not recommended. Execute through the
%   CALCULO script with the suitable flags to do the calculation.
%
%   This function only exists to save linespace in the CALCULO script.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



figure();
hold on;

II=NaN(size(I));
for ii=1:length(I)
for jj=1:length(I)
if I(ii,jj)>2e5
II(ii,jj)=2e5;
else
II(ii,jj)=I(ii,jj);
end
end
end

imagesc(II);

% Formatting
axis image;
colormap jet;
box on;
axis xy;
xlabel('x pixel');
ylabel('y pixel');

% Use title to list angle settings
if strcmp(geometry,'euler')==1
    title(strcat('$\omega=',num2str(omega),'\quad\chi=', ... 
                num2str(angle2),'\quad\varphi=',num2str(phi), ...
                '\quad\theta=',num2str(theta),'\quad D=', ...
                num2str(D),'$ mm'),'Interpreter','latex');
elseif strcmp(geometry,'kappa')==1
     title(strcat('$\omega=',num2str(omega),'\quad\kappa=', ... 
                num2str(angle2),'\quad\varphi=',num2str(phi), ...
                '\quad\theta=',num2str(theta),'\quad D=', ...
                num2str(D),'$ mm'),'Interpreter','latex');
end

% Colourbar
colbarhand = colorbar;
colbarhand.Label.String = 'Arbitrary units';

% Draw beamstop if it is visible
if sum(isnan([X0,Y0]))==0
    BSang = linspace(0,360,13)';
    BSvect = length(II)/24*[cosd(BSang),sind(BSang)];
    fill(X0+BSvect(:,1),Y0+BSvect(:,2),'w');
end

hold off;



end


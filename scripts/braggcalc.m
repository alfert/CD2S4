function [I] = braggcalc(Q,A,B,C,FWHM)
%BRAGGCALC Calculates intensity due to Bragg peaks.
%
%   Inputs:
%       Q       A 3D column vector in reciprocal space.
%       A,B,C   Three 3D non-coplanar reciprocal lattice column vectors.
%       FWHM    The instrumental resolution, corresponding to the FWHM of
%               the Bragg peaks, measured in reciprocal space distance.
%
%   Outputs:
%       I       The normalised intensity due to Bragg peaks alone at this
%               given Q vector.
%
%   Note:
%       -       It is assumed that the resolution is sufficiently high that
%               distinct Bragg peaks are easily resolved. This code cannot
%               handle overlapping peaks.
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018



% Find nearest Bragg peak by hkl number.
nearpeak = round([ dot(Q,A)/(norm(A)^2) , dot(Q,B)/(norm(B)^2) , ...
                   dot(Q,C)/(norm(C)^2) ]);
               
% Convert to reciprocal coordinates
nearcoord = nearpeak(1)*A + nearpeak(2)*B + nearpeak(3)*C;

% Evaluate intensity factor as a Gaussian away from the nearest peak
I = exp(-(norm(Q-nearcoord))^2/(FWHM^2)/2);



end
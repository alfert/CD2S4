\documentclass[12pt,a4paper,twoside]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{xfrac}
\usepackage{tabto}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{tabto}
\usepackage{chemformula}
\usepackage{siunitx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\author{\rule[-0.5ex]{3cm}{1em} \\ \normalsize{\texttt{\rule[-0.5ex]{2cm}{1em}@\rule[-0.5ex]{1cm}{1em}.\rule[-0.5ex]{0.5cm}{1em}}}}
\title{Simulating single crystal x-ray diffraction patterns in direct space with \textsc{Cd2s4}}
\setlength{\parindent}{0in}

\makeatletter
\newlength\tdima
\newcommand\tabfill[1]{%
      \setlength\tdima{\linewidth}%
      \addtolength\tdima{\@totalleftmargin}%
      \addtolength\tdima{-\dimen\@curtab}%
      \parbox[t]{\tdima}{#1\ifhmode\strut\fi}}

\newcommand\mytabs{\hspace*{1em}\=\hspace{1em}\=\hspace{2em}}
\newenvironment{mysec}[1][\mytabs]
  {\begin{tabbing}#1\kill\ignorespaces}
  {\end{tabbing}}
\makeatother

\begin{document}
\maketitle

\begin{abstract}
Although there already exist numerous examples of simulation software for x-ray diffraction, the overwhelming majority operate in reciprocal space. While desirable, obtaining real data in reciprocal space can prove difficult for large, absorptive samples. Presented here is a program, provisionally named \textsc{Cd2s4}, written in \textsc{Matlab}, which attempts to simulate direct space images that a real detector would observe.
\end{abstract}

\tableofcontents

\section{Preface}
It is likely that this project has been tried numerous times before by numerous different research groups, given it's basic use even to confirm Bragg peaks. The main difficulty its realisation poses is the number of variables involved, and the precision required in their implementation. There is also the issue that x-ray diffractometers (XRDs) are not always forthright with many bits of rather essential information. At least, the one I used\footnote{\quad Rigaku Oxford Diffraction's \textit{Xcalibur 2} with \textit{Eos} CCD and \textit{CrysAlis Pro} controlling/analysis software} was not.\\

The original motivation for this project was to detect diffuse scattering. This is typically done in reciprocal space using neutron scattering which, although functional, is not exactly possible in a `home' lab. The ability to model crystallographic scattering in direct space would allow researchers to bypass the many issues associated with measuring in reciprocal space (namely absorption, and data completeness for large samples).\\

The program's name, \textsc{Cd2s4}, is an abbreviated, unordered acronym of the program's description: \textbf{D}irect \textbf{S}pace \textbf{S}ingle \textbf{C}rystal \textbf{D}iffraction \textbf{S}imulation \textbf{S}oftware (DSSCDSS). I make no claim that this is a good name, but it is functional, and that's what counts in the end! Any other name suggestions are welcome.

\subsection{Terminology}
\begin{mysec}
\hspace{2em}\=\hspace{2em}\=\kill
\>\textsf{\textbf{direct space}}\+\\ \> \tabfill{The fixed, real-spatial reference frame. Also known as the `laboratory frame'. A perfect crystal forms a regular lattice in \textsf{direct space}.}\\[0.5em]
\textsf{\textbf{reciprocal space}}\\ \> \tabfill{The space formed by the momentum transfer vector $\mathbf{Q}$ (see section 1.2). A perfect crystal scatters radiation in a regular lattice in \textsf{reciprocal space}.}\\[1.5em]
\textsf{\textbf{system axes}}\\ \> \tabfill{The fixed, right-handed, Cartesian coordinate system associated with the \textsf{direct space}. In this case, incident radiation moves in the $-\mathbf{\hat{x}}$ direction, and the detector rotates around $\mathbf{\hat{z}}$ (vertically upwards).}\\[0.5em]
\textsf{\textbf{bulk axes}}\\ \> \tabfill{A right-handed, Cartesian coordinate system in \textsf{direct space} rigidly attached to the macroscopic crystal. When the goniometer angle settings $\varphi=\kappa=\omega=0$, the \textsf{bulk axes} and the \textsf{system axes} are identical.}\\[0.5em]
\textsf{\textbf{cell axes}}\\ \> \tabfill{A right-handed, Cartesian coordinate system in \textsf{direct space} associated with the crystal's \textsf{unit cell}. A \textsf{bulk axes} vector $\mathbf{r_b}$ is related to a \textsf{cell axes} vector $\mathbf{r_c}$ via $\mathbf{r_b}=\mathsf{U}\,\mathbf{r_c}$ (see section 1.2).}\\[0.5em]
\textsf{\textbf{basis axes}}\\ \> \tabfill{A right-handed coordinate system in \textsf{direct space} associated with the crystal's \textsf{unit cell}. A \textsf{cell axes} vector $\mathbf{r_c}$ is related to a \textsf{basis axes} vector $\mathbf{h}$ via $\mathbf{r_c}=\mathsf{B}\,\mathbf{h}$, where $\mathsf{B}$ is defined in Busing \& Levy 1966\footnotemark[2]. In a cubic crystal, the \textsf{basis axes} and the \textsf{cell axes} are parallel, with the latter scaled by \texttt{latparam}.}
\end{mysec}
\stepcounter{footnote}\footnotetext{\quad W. R. Busing \& H. A. Levy, \textit{Angle Calculations for 3- and 4- Circle X-ray and Neutron Diffractometers}, Acta. Cryst. (1967). \textbf{22}, 457}
\pagebreak


\subsection{Definitions}
\begin{mysec}
\hspace{2em}\=\hspace{6em}\=\kill
\>$\mathbf{v}$ \> A vector in some $\mathbb{R}^3$ space.\+\\[0.5em]
$\mathbf{\hat{v}}$ \> A unit vector in some $\mathbb{R}^3$ space, parallel to $\mathbf{v}$.\\[0.5em]
$v$\> The magnitude of $\mathbf{v}$.\\[0.5em]
$\mathsf{M}$ \> A $3\times 3$ matrix.\\[0.5em]
\texttt{array\lbrack j,i\rbrack} \> The cell located in the $i$-th column and the $j$-th row of \texttt{array}.\\[1.5em]
$\lambda$ \> The wavelength of incident radiation.\\[0.5em]
$\mathbf{k}$ \> A wavevector, where $k=2\pi/\lambda$.\\[0.5em]
$\mathbf{Q}$ \> \tabfill{The momentum transfer vector in elastic scattering, where $\mathbf{Q}=\mathbf{k_f}-\mathbf{k_i}$ is the change in wavevector during scattering.}\\[1.5em]
$\mathbf{a}, \mathbf{b}, \mathbf{c}$ \> The crystal lattice basis vectors.\\[0.5em]
$\mathbf{A}, \mathbf{B}, \mathbf{C}$ \> The reciprocal lattice basis vectors.\\[0.5em]
$\mathbf{r}$ \> A vector to the position of some atom in terms of $\mathbf{a}$, $\mathbf{b}$, and $\mathbf{c}$.\\[1.5em]
$\mathsf{U}$ \> The orientation matrix\footnotemark[2] .\\[0.5em]
$\varphi$, $\kappa$, $\omega$ \> \tabfill{The three angles about whose axes a `kappa' diffractometer can orient the sample.}\\[0.5em]
$\mathsf{\Phi}, \mathsf{K}, \mathsf{\Omega}$ \> \tabfill{The three $3\times 3$ rotation matrices corresponding to the $\varphi$, $\kappa$, and $\omega$ angles and associated axes respectively.}\\[0.5em]
$\theta$ \> \tabfill{The angle a line, from the sample to the centre of the detector, makes with the beamline. Note that this is sometimes referred to as $2\theta$.}\\[0.5em]
$D$ \> The distance from the sample to the centre of the detector.\\[1.5em]
$N$ \> \tabfill{The number of pixels along each side of the detector; the image has dimensions $N\times N$.}\\[0.5em]
$W$ \> The side-length of the detector's active region.\\[1.5em]
\texttt{I} \> \tabfill{An $N\times N$ array whose cells, \texttt{I\lbrack j,i\rbrack}, hold values which correspond to the detector pixel's intensity, on a one-to-one basis.}\\[0.5em]
\texttt{InstrRot} \> A $3\times 3$ matrix equal to the product $\mathsf{\Phi K\Omega}$.\\[0.5em]
\texttt{latparam} \> The lattice parameter.\\[0.5em]
\texttt{atoms} \> \tabfill{A $3\times n$ array, where the positions of the $n$ atoms are contained. Each column represents the position vector of a single atom according to the system axes.}\\[0.5em]
\texttt{FWHM} \> \tabfill{A semi-arbitrary scaling of the broadness of each Bragg peak due to experimental limitations. It is technically the full width at half-maximum of a Gaussian, measured in reciprocal space.}\\[0.5em]
\texttt{threshold} \> \tabfill{A value between 0 and 1 which determines the largest Bragg intensities that are ignored.}\\[1.5em]
\< Any other constant or variable mentioned has limited scope and is defined when introduced.
\end{mysec}

\section{Usage}
\subsection{Installation}
To install, simply add the \textsc{Cd2s4} folder to your \textsc{Matlab} environment's path (via \texttt{Set Path} button, under the \texttt{Environment} group of the \texttt{Home} menu tab), or copy all files into a directory which is already included in the path.\\

To run the program, first predefine the variables listed in \texttt{calculo.m}. Then, simply call \texttt{calculo} from within the \textsc{Matlab} command line. If you do not yet have a parallel pool running (see section 2.2), you may need to wait around 30 seconds longer while the pool is initialised.\\

All code can be found within the \texttt{scripts} folder. Each file contains a comment header explaining its operation and usage. The `top level' script is \texttt{calculo.m}, and should be your first one to read. The only script which does not branch out from \texttt{calculo.m} is \texttt{drawcell.m}.

\subsection{Dependencies}
The program was optimised so that the main bulk of its calculation could be run in parallel. This requires \textit{MathWork}'s \textit{Parallel Computing Toolbox}\footnote{\quad\url{https://www.mathworks.com/products/parallel-computing.html}}, which is not necessarily included in the base program, and, regardless, requires a separate download, accessible through the `Add-ons' menu. If you lack access to this dependency, it is possible to work around it:
\begin{enumerate}
\item Remove the \texttt{gcp} command on line 18 of \texttt{docomputation.m}.
\item Remove lines 25 and 26 of \texttt{docomputation.m}.
\item Change the \texttt{parfor} command on line 28 of \texttt{docomputation.m} to \texttt{for}.
\item Remove the \texttt{send(\ldots)} command on line 33 of \texttt{docomputation.m}. This will cause the loading bar to not update during the bulk of the computation; this can be fixed by adding in \texttt{waitbar(3/64+7*Y/N/8,loadbar)} where the \texttt{send(\ldots)} was previously.
\item Change the \texttt{parfor} command on line 49 of \texttt{drawXRD.m} to \texttt{for}.\\
\end{enumerate}



\section{Method overview}
\textsc{Initialisation}\\
\tabto{1cm}\textbf{Check} all variables are valid\\
\tabto{1cm}\textbf{Set} invalid variables to \textit{default}\\
\tabto{1cm}\textbf{Rotate} basis vectors as per \textit{current settings}\\
\tabto{1cm}\textbf{Calculate} derived variables\\

\textsc{Main calculation}\\
\tabto{1cm}\textbf{For each} pixel in \textit{the detector}\\
\tabto{2cm}\textbf{Calculate} the Q vector\\
\tabto{2cm}\textbf{Calculate} the intensity factor due to proximity to \textit{Bragg peaks}\\
\tabto{2cm}\textbf{If} the \textit{Bragg intensity} is above the \textit{threshold}
\tabto{3cm}\textbf{Calculate} the intensity factor due to the \textit{structure factor}\\
\tabto{3cm}\textbf{Set} the pixel value to the \textit{Bragg intensity \texttimes\ structure factor}\\
\tabto{2cm}\textbf{Else}\\
\tabto{3cm}\textbf{Set} the pixel value to \textit{zero}

\textsc{Beamstop}\\
\tabto{1cm}\textbf{If} the \textit{straight-through} is incident on \textit{the detector}\\
\tabto{2cm}\textbf{Set} a small region around it to \textit{zero}\\

\textsc{Image plotting}\\
\tabto{1cm}\textbf{Plot} the pixels as an image\\
\tabto{1cm}\textbf{If} the \textit{straight-through} is incident on \textit{the detector}\\
\tabto{2cm}\textbf{Draw} a beamstop on top of the image\\
\tabto{1cm}\textbf{Apply formatting} to image\\

\textsc{Schematic drawing}\\
\tabto{1cm}\textbf{Calculate} positions of the instrument as per \textit{current settings}\\
\tabto{1cm}\textbf{Draw} system, crystal bulk, and crystal cell axes\\
\tabto{1cm}\textbf{Draw} cylindrical crystal representation\\
\tabto{1cm}\textbf{Draw} detector screen



\section{Algorithm}
Although I believe my code is reasonably clear, with appropriate headers and comments throughout, here's a complete explanation anyway.

\subsection{Initialisation}
\subsubsection{The unit cell}
Currently, the unit cell is manually defined via the \texttt{ybtio.m} file, which defines the cell structure for ytterbium titanate, \ch{Yb_{2}Ti_{2}O_{7}}. The script generates an array \texttt{r}, whose columns contain the fractional position of each atom in the perfect unit cell, as a row vector. It also defines the lattice parameters \texttt{latparam}, which simply scales \texttt{r} to direct space distances.\\

The most important thing to note is that every atom in the unit cell is included in \texttt{r}, regardless of its atomic number. Commenting in \texttt{ybtio.m} shows which rows are for which atom; when each atom needs to be treated separately (see section 4.2.2), the necessary behaviour is currently hardcoded. 

\subsubsection{Rotations and orientations}
This is where a lot of confusion begins. There are two primary angle conventions used in XRDs: Euler angles, and, if applicable, the native `kappa' geometry. The first thing to note is that both of these rotations are left-handed; that is, when the axis vector points towards the viewer, the rotation is clockwise. Euler angles are defined as usual (besides handedness), and are more natural for four- and six-circle diffractometers. On the other hand, `kappa' diffractometers have three independent, connected arms, which together can access all orientations. The three angles are, conventionally, called $\varphi$, $\kappa$ (contrast with Euler angles' $\chi$), and $\omega$.\\

The $\omega$ rotation is clockwise about the $\mathbf{\hat{z}}$ direction, and does not change. The $\varphi$ rotation is also about the $\mathbf{\hat{z}}$ direction, but only when $\kappa=0$, since the $\varphi$ axis is itself dependent on the $\kappa$ angle. The $\kappa$ axis is in the $xz$ plane, at some angle to the $\mathbf{\hat{x}}$ axis conventionally called $\alpha$, which varies between XRD model. There is also a misalignment angle $\beta$, which measures the angle between the $\hat{\kappa} z$ plane and the $xz$ plane. The $\kappa$ axis is itself situated on the $\omega$ arm, and so rotates with it.\\

The above formalism can be represented as a series of three standard matrix rotations. Let the bulk axes be initially aligned with the system axes. First they are rotated clockwise about the $\mathbf{\hat{z}}$ axis by $\varphi$. Then, they are rotated clockwise about the $\kappa$ axis by $\kappa$ (since this axis is not aligned with any of the system axes its form is not trivial). Finally, they are rotated clockwise about the $\mathbf{\hat{z}}$ axis again by $\omega$. Each of these matrices are represented by $\mathsf{\Phi}$, $\mathsf{K}$, and $\mathsf{\Omega}$ respectively.\\

In the code, each of these matrices are made using the \texttt{genrot(\ldots)} custom function, which takes an angle and axis to return the right-handed rotation matrix. The three matrices are multiplied together in the right order to form \texttt{InstrRot}. Since the orientation matrix \texttt{U} transforms from the cell axes to the bulk axes, by forming the product $\mathsf{\Omega\,K\,\Phi\,U}$ a full transformation from cell axes to system axes can be made.\\

This matrix acts upon the cell axes (which are initially the system axes scaled by \texttt{latparam}) to rotate them into their proper positions relative to the system axes, taking into account both the crystal's microscopic orientation and the macroscopic orientation due to the goniometer settings. The resulting three vectors are saved as \texttt{a},\texttt{b}, and \texttt{c}, the direct space basis vectors. The \texttt{atoms} array is also formed (see section 1.2) by similar means.

\subsubsection{Everything else}
Remaining variables, if not already defined, are simply set to a somewhat arbitrary default value. Many \texttt{exist(\ldots)} commands are called to achieve this. Default values can be obviously changed trivially.\\

Derived variables such as $\mathbf{k}$, and the reciprocal lattice vectors $\mathbf{A}$, $\mathbf{B}$, and $\mathbf{C}$, are calculated at this point. The only non-trivial part here is the \texttt{reciprogen(a,b,c)} function, which returns the reciprocal lattice vectors by applying their definition directly.


\subsection{Intensity calculations}
The overwhelming majority of the program's runtime is spent on this section. To allow the loading bar to continue to update throughout the parallelised process, a hack was made using the function \texttt{docomputation(\ldots)}. It works by creating its own variable space, shared with the subfunction \texttt{updatewaitbar()}. Each time one row of the resultant image is computed, the worker calls \texttt{updatewaitbar()}, which, as expected, updates the loading bar, and increments a counter. This counter is required because the iterations in a \texttt{parfor} loop are not necessarily completed in order. Since \texttt{updatewaitbar()} always has access to \texttt{counter} and \texttt{N} in its private space, no data needs to be sent with the command.\\

Mathematically, the intensity of each pixel is the product of two factors: the Bragg peaks, and the structure factor. Since these are two independent factors, they are computed separately with two different functions described below. However, they rely upon the scattering vector, $\mathbf{Q}$, so a conversion function \texttt{pixel2Q(\ldots)} is called. This function works by working out the position of the pixel in direct space, then rotating the resultant vector about the $\mathbf{\hat{z}}$ axis by $\theta$.\\

The whole section is optimised via the \texttt{parfor} command, which executes different iterations of a normal \texttt{for} loop in parallel. Each pixel is independent, but there will typically be more pixel rows than workers (independent CPUs), so paralleling the outer layer is sufficient.

\subsubsection{The Bragg peaks}
Theoretically, a perfect, infinite crystal with no phonons would scatter light into delta functions. Since none of these criteria necessarily apply in reality, each Bragg peak has some amount of width. This is modelled as a 3D Gaussian in reciprocal space, with full-width at half maximum defined by \texttt{FWHM}. This is ultimately an arbitrary scaling, and should be chosen by comparing test simulations to empirical data.\\

The \texttt{braggcalc(\ldots)} function first takes the dot product between $\sfrac{\mathbf{Q}}{A}$ and $\mathbf{\hat{A}}$ to find the nearest peak's index number along $\mathbf{A}$; the same occurs for both $\mathbf{B}$ and $\mathbf{C}$. It then rescales this index number back to a position in reciprocal space as measured in units of $\si{\angstrom^{-1}}$. Now that the peak centre is known, the intensity due to the Gaussian can be computed directly as the difference between this this position and the $\mathbf{Q}$ vector itself.\\

The returned value will be somewhere between 0 and 1. If it is below \texttt{threshold}, then section 4.2.2 is skipped and the pixel value is immediately set to zero. To prevent this behaviour without losing computational speed when \texttt{threshold}=0, remove lines 32--34 and 36 in \texttt{docomputation.m}.

\subsubsection{The structure factor}
The value of $Q$ for x-ray diffraction will not typically exceed \SI{16}{\angstrom^{-1}}, which is well within the bounds for the well-known approximation of the form factor as the sum of four Gaussians\footnote{\quad P. J. Brown \textit{et al.}, \textit{Intensity of diffracted intensities}, International Tables for Crystallography (2006), Vol. C, ch. 6.1, pp. 554--595}. The parameters for each of these four Gaussians were hardcoded into the function, and organised as per the expected elements and position of said elements within the \texttt{atoms} array.\\

The form factor is calculated, from which the structure factor is calculated as usual.

\subsubsection{The beamstop}
The part of the detector -- if any -- which is incident to the beam with $Q=0$ is typically far brighter than the other features. For this reason, an opaque object is placed between the sample and the detector to absorb this bright region. The same effect occurs in the simulation; the addition of an artificial beamstop also gives a distincitive reference point from which empirical data can be compared.\\

First, the \texttt{Q2pixel(\ldots)} function is called, which performs the opposite of \texttt{pixel2Q(\ldots)}. It identifies the point on the detector which receives the straight-through beam and saves its coordinates as \texttt{X0} and \texttt{Y0}; if no such point exists, \texttt{X0} and \texttt{Y0} are set to \texttt{NaN}. An arbitrarily-sized, square region of cells around this point in the output array \texttt{I} is then set to zero.

\subsection{Plotting the image}
An optional flag, \texttt{nodraw}, can be defined, which, when activated, suppresses the image plotting. In order to keep the number of lines in each segment low, the image generation subroutine is delegated to the \texttt{plotgraph(\ldots)} function.\\

This function creates an empty figure, plots the image array \texttt{I} with a scaled colourmap, applies generic formatting, and then, if applicable, draws on a white beamstop. This is done by checking if \texttt{X0} and \texttt{Y0} are \texttt{NaN}; if they are not, then the vertices of a dodecagon (arbitrary shape) are calculated with the unperturbed beam at its centre, which is then filled.

\subsection{Drawing the schematic}
This subroutine was made as a verification method that the geometry and positions \textsc{Cd2s4} is using matches up with the physical XRD, if data is being compared. If the settings are trusted, then another optional flag, \texttt{noplot}, can be defined, which, when activated, suppresses the schematic drawing. Otherwise, the \texttt{drawXRD(\ldots)} function is called.\\

After creating an empty figure, it draws unscaled arrows for the system, bulk, and cell axes, and places a floating text label below each one. It then generates a cylinder to represent the crystal (a purely arbitrary choice), and marks the `top' and `front' of the cylinder with black lines. It then generates a flat rectangle to represent the detector surface.\\

There are numerous rotations involved here, and the base coordinates are regularly grouped into matrices for ease of computation. The actual coordinate values prior to rotation are also arbitrary, and chosen primarily for visual clarity. In general, multiples of 4 were used for position, and lengths are all integer values. Colouring is also arbitrary.

\subsection{Tidying up}
The last segment of \texttt{calculo.m} is fairly self-explanatory. Since it runs as a script, all variables are saved to the main environment. To prevent clutter, some variables are deleted via the \texttt{clear} command. Remove these lines to suppress the chosen variable from being deleted.

\section{Recommendations for future progress}
\begin{mysec}
\hspace{2em}\=\hspace{2em}\=\kill
\>\textbf{Half-lambda reflections}\+\\ \> \tabfill{Incident radiation in practice is not purely monochromatic, and higher harmonics are also present, primarily radiation of wavelength $\lambda/2$. By also computing the diffraction pattern for this wavelength, and then scaling the intensity to be in empirical proportion to the $\lambda$ wavelength, a more accurate model can be obtained. This will be easy to implement, but effectively doubles the computation time, so some means of suppressing it should be included.}\\[1em]
\textbf{Euler angles}\\ \> \tabfill{Despite currently being coded, the Euler angle conversion seems to not be accurate. I personally have no idea why any more, but since the XRD itself converts from its native angles to Euler to begin with, it may be easier to simply use the kappa-geometry. Nonetheless, it may be useful if the code is to be published for others to use, since Euler angles are, of course, fairly universal.}\\[1em]
\textbf{Generalised coordinate systems}\\ \> \tabfill{All angles and directions are currently defined for the XRD used to test the \textsc{Cd2s4} prototype\footnotemark[1]. Although altering convention is fairly simple thanks to \texttt{genrot(\ldots)}, it could be useful to add compatability for some common alternative conventions.}\\[1em]
\textbf{Generalised atomic composition}\\ \> \tabfill{The current system for defining \texttt{r} is rather restrictive, in that it requires hardcoding in a very specific manner. In order to generalise it, the array should be expanded to have 4 columns, with the fourth labelling each atom with its elemental symbol. By using this and a form factor lookup table, the structure factor can be calculated without this hardcoding. This method also allows substitutional defects to be easily added by hand.}\\[1em]
\textbf{Generalised cell shape}\\ \> \tabfill{Currently, only cubic, tetragonal, and orthorhombic systems can be used with \textsc{Cd2s4}. The \texttt{r} structure does allow different geometries; it simply requires coding in multiple values of what is currently solely \texttt{latparam}, and also specifying axes and/or angles.}\\[1em]
\textbf{Supercells}\\ \> \tabfill{As the program currently stands, a supercell can be used by simply replacing \texttt{ybtio} or \texttt{r} with the appropriate coordinate matrix, and scaling \texttt{latparam} appropriately. However, this will require all atoms of the same element to be grouped, which may be convoluted depending on the supercell generation method and size. For this reason, it may be wise to only begin testing supercells once general atomic compositions can be catered.}\\[1em]
\textbf{Importing \texttt{.cif} files}\\ \> \tabfill{A fairly self-explanatory suggestion that would greatly assist with usage for those not using \ch{Yb_{2}Ti_{2}O_{7}}. This should only be done after general atomic compositions can be accommodated, though.}\\[1em]
\textbf{Runtime estimation}\\ \> \tabfill{Although the program runs in $\mathcal{O}\left(N^2\right)$ time for an image of $N\times N$ pixels, the proportionality constant of course varies entirely on the size of the cell. By simply testing a number of different sizes for a number of different values of $N$, a good predictive model for the runtime could be produced. That said, since this is primarily just a usability flair without any practical use, it should not be highly prioritised.}\\[1em]
\textbf{GUIs and application bundling}\\ \> \tabfill{I have no experience with designing or coding GUIs, nor producing stand-alone applications. However, I know that MathWorks offers an albeit limited \textsc{Matlab}-to-C converter, and this will allow the code to be easily used on different platforms. Even in the early stage right now, juggling the various variables and functions within the same \textsc{Matlab} environment is becoming complex!}
\end{mysec}

\section{Postface}
I may (am?) slowly work through the above recommendations myself, but, of course, I can't give any specific deadlines. This file will be modified as progress is made. If anything looks incorrect, out-of-place, or otherwise erroneous (including spelling/grammar issues), please do tell me or flag an issue on the project page\footnote{\quad \url{https://gitlab.com/}\rule[-0.5ex]{2cm}{1em}\url{/CD2S4}}.\\

I hope this made sufficient sense! Please let me know if you end up using this for any purpose at all---I'd be very glad to see it come to use. Otherwise, feel free to email me if you have any comments, complaints, or compliments.

\section{Legal stuff}
\tiny{
Copyright (c) 2018 \rule[-0.5ex]{2cm}{1em} \textit{(the author)}\\

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\\[-0.5em]

\hspace{2em}The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.\\

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
}\\

\normalsize{This may get `upgraded' to the GNU GPLv3 license (or anything else!) if/when the project becomes sufficiently advanced, but right now I'm fairly ambivalent about the code's usage.}

\end{document}
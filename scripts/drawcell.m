% Draws the full unit Yb2Ti2O7 cell.
% This is not used in any calculations and was just a test to make sure
% that I had copied over VESTA's coordinates properly. 
%
%   Author:     ---
%   Email:      ---
%   Date:       27/07/2018


ybtio;
[Xxx,Yyy,Zzz] = sphere;
Xxx = 0.8*Xxx;
Yyy = 0.8*Yyy;
Zzz = 0.8*Zzz;

figure();
hold on;
grid;
axis equal;

for iter8=1:15
   surf(Xxx+8*r(iter8,1),Yyy+8*r(iter8,2),Zzz+8*r(iter8,3), ...
        'FaceColor','y');
end
handy = surf(Xxx+8*r(16,1),Yyy+8*r(16,2),Zzz+8*r(16,3), ...
             'FaceColor','y');
         
for iter8=17:31
   surf(0.8*Xxx+8*r(iter8,1),0.8*Yyy+8*r(iter8,2),0.8*Zzz+8*r(iter8,3), ...
        'FaceColor','b');
end
handb = surf(0.8*Xxx+8*r(32,1),0.8*Yyy+8*r(32,2),0.8*Zzz+8*r(32,3), ...
             'FaceColor','b');
         
for iter8=33:size(r,1)-1
   surf(0.4*Xxx+8*r(iter8,1),0.4*Yyy+8*r(iter8,2),0.4*Zzz+8*r(iter8,3), ...
        'FaceColor','r');
end
handr = surf(0.4*Xxx+8*r(end,1),0.4*Yyy+8*r(end,2),0.4*Zzz+8*r(end,3), ...
             'FaceColor','r');
         
plot3([0,8],[0,0],[0,0],'k');
plot3([8,8],[0,8],[0,0],'k');
plot3([8,8],[8,8],[0,8],'k');
plot3([8,0],[8,8],[8,8],'k');
plot3([0,0],[8,0],[8,8],'k');
plot3([0,0],[0,0],[8,0],'k');
plot3([0,0],[0,8],[0,0],'k');
plot3([0,0],[8,8],[0,8],'k');
plot3([8,8],[0,0],[0,8],'k');
plot3([8,8],[0,8],[8,8],'k');
plot3([0,8],[0,0],[8,8],'k');
plot3([0,8],[8,8],[0,0],'k');
plot3([0,0],[0,0],[0,-2],'k--');
plot3([8,8],[0,0],[0,-2],'k--');
plot3([0,0],[8,8],[0,-2],'k--');
plot3([8,8],[8,8],[0,-2],'k--');
plot3([8,10],[0,0],[0,0],'k--');
plot3([0,0],[8,10],[0,0],'k--');
plot3([8,8],[8,10],[0,0],'k--');
plot3([8,10],[8,8],[0,0],'k--');
plot3([0,0],[0,0],[8,10],'k--');
plot3([8,8],[0,0],[8,10],'k--');
plot3([8,8],[0,-2],[8,8],'k--');
plot3([0,0],[8,8],[8,10],'k--');
plot3([8,8],[8,8],[8,10],'k--');
plot3([8,10],[0,0],[8,8],'k--');
plot3([0,0],[8,10],[8,8],'k--');
plot3([8,8],[8,10],[8,8],'k--');
plot3([8,10],[8,8],[8,8],'k--');
plot3([0,-2],[0,0],[8,8],'k--');
plot3([0,0],[0,-2],[8,8],'k--');
plot3([0,-2],[8,8],[8,8],'k--');
plot3([0,-2],[8,8],[0,0],'k--');
plot3([8,8],[0,-2],[0,0],'k--');
plot3([0,0],[0,-2],[0,0],'k--');
plot3([0,-2],[0,0],[0,0],'k--');
dfdf
axis([-2,10,-2,10,-2,10]);
view(0,0);
xlabel('a');
ylabel('b');
zlabel('c');
legend([handy,handb,handr],'Yb^{3+}','Ti^{4+}','O^{2-}');
hold off;

clear Xxx;
clear Yyy;
clear Zzz;
clear iter8;
clear handy;
clear handb;
clear handr;